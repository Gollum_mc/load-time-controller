# Load Time Controller

A powerful tool for web admins to monitor their sites' performance in comaprison to others.

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes.

### Prerequisites

The app requires Python 3.7

### Installing

Create a virtual environment for the project, Linux example below:

```
virtualenv -p python3.7 <name of project>
```

go into the project folder

```
cd <name of the project>
```

clone project repository
```
git clone https://Gollum_mc@bitbucket.org/Gollum_mc/load-time-controller.git
```

activate virtual environment
```
source bin/activate
```

go to the 'load_time_controller' directory:
```
cd load_time_controller/
```

install depedencies
```
python setup.py develop
```

then run the app and follow the instruction displayed in the console:

```
python application.py
```
### SMS, email recipient configuration
if you want to change sms and email recipient data go to **settings.py** and update SMS_RECIPIENT and EMAIL_RECIPIENT:

## How it works

**Load Time Controller** is a very simple console-based app.
When starting it will ask you to type in a main URL that is going to be tested.
You can type in for example:
```
http://youtube.com
```
Then it will ask for a list of unique benchmark URLs that are going to be compared with the main one. Please, type them in one line, 
separate using commas, and do not forget about a proper URL format http(s)://example.com. You can type in for example:

```
http://google.com, http://ebay.com, http://yahoo.com
```
Then the app starts testing and comparing sites' loading time, each step will be printed to a console, so you will know what is currently happening.
Each test result is saved in **reports.txt** file in 'logs' directory.
In case that at least one of benchmark sites loads faster than the main site, the app will send an email.
In case that at least one of benchmark sites loads two times faster than the main site, the app will additionally notify by a SMS.
In the end the app will print a table presenting test results and comparison.

Enjoy using Load Time Controller!
## Running the tests

Tests are written using Unittest module. In order to run it, write in your python command and name of a test file you are interested in.
Tests can be also ran via your IDE.
Go to the tests folder:
```
python test_url_manager.py
```

## Authors

* **Marta Chludzińska**

