from logger import Logger
from services.url_manager import UrlManager
from services.caller import Caller
from services.reporter_factory import ReporterFactory, ReporterType
from services.alerts_manager import AlertManager
from models.errors import Error


Logger.configure_logs()
url_manager = UrlManager()

caller = Caller()
logger = Logger()
alert_manager = AlertManager()

class Application:

    @staticmethod
    def run():
        what_to_do = input("Choose option:\n 1 - compare sites loading times\n 2 - quit\n")
        if what_to_do == str(1):
            app.manage()
        else:
            app.app_exit()

    @staticmethod
    def manage():
        urls = url_manager.save_main_url()
        all_urls = url_manager.save_benchmark_url(urls)
        if caller.is_network_connection():
            url_data_dicts = url_manager.get_url_data_dicts(all_urls)
            reporter = ReporterFactory.get_reporter(ReporterType.Basic.value)
            reports, main_report, exceeded_times_tuples, twice_exceeded_times_sites_no = reporter.generate_report_data(
                url_data_dicts)
            AlertManager.send_alert(exceeded_times_tuples, twice_exceeded_times_sites_no, main_report)
            reporter.generate_report(reports)
        else:
            Error.print_errors([Error.NO_INTERNET_CONNECTION.value])
            app.app_exit()

    @staticmethod
    def app_exit():
        exit()

app = Application()

if __name__ == '__main__':
    app.run()


