import logging

class Logger:
    LOGGERS = [
        (logging.getLogger("reports"), "reports", logging.INFO)
    ]

    @classmethod
    def configure_logs(cls):
        import logging.handlers
        import os

        path = lambda root, *a: os.path.join(root, *a)
        ROOT = os.path.dirname(os.path.abspath(__file__))
        LOG_FOLDER = 'logs'

        fmt = '%(message)s'

        formatter = logging.Formatter(fmt=fmt)

        for log_info in Logger.LOGGERS:
            logger = log_info[0]
            logger.setLevel(log_info[2])
            if not logger.handlers:
                logHandler = logging.FileHandler(os.path.join(ROOT, LOG_FOLDER, log_info[1] + ".txt"), encoding='utf-8')
                logHandler.setFormatter(formatter)
                logger.addHandler(logHandler)

