import enum
from termcolor import colored

class Error(enum.Enum):
    WRONG_URL = "Wrong URL format."
    URL_ALREADY_ADDED = "You have already added such url."
    URL_NOT_REACHABLE = "Incorrect URL."
    MAIN_SITE_UNREACHABLE = "Main site is unreachable. Can not continue."
    CAN_NOT_SEND_SMS = "Not enough points to send SMS."
    NO_INTERNET_CONNECTION = "No internet connection"


    @staticmethod
    def print_errors(errors):
        for error in set(errors):
            print(colored(error, 'red'))