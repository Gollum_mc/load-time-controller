from dataclasses import dataclass
from datetime import datetime


@dataclass
class Report:
    url: str
    is_main: bool
    time: float
    test_datetime: datetime
    response_code: int
    error: str
    time_delta: float = None