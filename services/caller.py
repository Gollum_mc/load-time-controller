from urllib import request
from urllib import error as urllib_error
from services.timer import Timer
from models.errors import Error


class Caller:

    TIMEOUT_SECONDS = 5

    @staticmethod
    def is_network_connection():
        try:
            request.urlopen('http://google.com', timeout=Caller.TIMEOUT_SECONDS)
        except urllib_error.URLError:
            return False
        return True

    @staticmethod
    def get_calls_results(url_data_dicts):
        results = []
        for url_dict in url_data_dicts:
            results.append(Caller.get_response(url_dict))
        return results

    @staticmethod
    def get_response(url_dict):
        print("Calling {}...".format(url_dict['url']))
        response, test_datetime, time, error = Caller.call_url(url_dict['url'])

        if url_dict['is_main']:
            Caller.verify_main_url_success(response)

        return dict(url=url_dict['url'], is_main=url_dict['is_main'], time=time, test_datetime=test_datetime,
                    code=response.status if response else 404, error=error)

    @staticmethod
    def verify_main_url_success(response):
        from application import app
        if not response or response.status != 200:
            Error.print_errors([Error.MAIN_SITE_UNREACHABLE.value])
            app.app_exit()

    @staticmethod
    def call_url(url):
        response = None
        e = None
        with Timer() as timer:
            print("Measuring time...")
            try:
                response = request.urlopen(url, timeout=Caller.TIMEOUT_SECONDS)
            except urllib_error.URLError:
                e = Error.URL_NOT_REACHABLE.value
            except ValueError:
                e = Error.URL_NOT_REACHABLE.value
        time = timer.time_delta if not e else None
        test_datetime = timer.test_datetime
        return response, test_datetime, time, e
