import smtplib
import time
from datetime import datetime
from email import utils
from email.header import Header
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from smsapi.client import SmsApiPlClient
from smsapi.exception import SendException, SmsApiException
from models.errors import Error
from settings import SMS_RECIPIENT

SMS_API_TOKEN = '75GZYyPnJt3XdBDurbt1XFYji6J8FqvUsx8LCYtX'


class SMTPNotification:

    def __init__(self, recipient, subject, template, reply_to=None):
        self.fromaddr = 'Alert <test.mail.notification999@gmail.com>'
        self.username = 'test.mail.notification999@gmail.com'
        self.password = 'Test@123'
        self.recipient = recipient
        self.reply_to = reply_to

        msg = MIMEMultipart('alternative')
        msg.set_charset('utf8')
        msg['FROM'] = self.fromaddr
        msg['Subject'] = Header(subject.encode('utf-8'), 'UTF-8').encode()
        msg['To'] = self.recipient
        msg['Date'] = SMTPNotification.get_date()
        if self.reply_to:
            msg['Reply-To'] = self.reply_to
        _attach = MIMEText(template.encode('utf-8'), 'html', 'UTF-8')
        msg.attach(_attach)

        self.message = msg

    @staticmethod
    def get_date():
        now_date = datetime.now()
        nowtuple = now_date.timetuple()
        now_timestamp = time.mktime(nowtuple)
        date_header = utils.formatdate(now_timestamp)
        return date_header

    def send(self):
        print("Sending email...")
        self.action()

    def action(self):
        server = smtplib.SMTP('smtp.gmail.com', 587)

        server.ehlo()
        server.starttls()
        server.login(self.username, self.password)
        server.sendmail(self.fromaddr, self.recipient, self.message.as_string())
        server.quit()


class SMSNotification:
    CLIENT = SmsApiPlClient(access_token=SMS_API_TOKEN)

    def __init__(self, message):
        self.message = message

    def send_sms(self):

        points = SMSNotification.get_sms_api_points()
        if points >= 0.16:
            try:
                print("Sending sms...")
                response = SMSNotification.CLIENT.sms.send(to=SMS_RECIPIENT, message=self.message, encoding='utf-8')
                print(response)
            except SendException as e:
                print(e.message)
                pass
            except SmsApiException:
                pass
        else:
            print(Error.CAN_NOT_SEND_SMS.value)

    @classmethod
    def get_sms_api_points(cls):

        try:
            points = SMSNotification.CLIENT.account.balance().points
        except SmsApiException:
            points = 0

        return points