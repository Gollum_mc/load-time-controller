import enum
from services.basic_reporter import BasicReport

class ReporterType(enum.Enum):
    Basic = "basic"


class ReporterFactory:

    @classmethod
    def get_reporter(cls, type):
        if type == ReporterType.Basic.value:
            return BasicReport()