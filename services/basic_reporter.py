from services.reporters import Reporter
import logging
import termtables


class BasicReport(Reporter):
    LOG = 'reports'


    @staticmethod
    def generate_report(reports_objects):
        print("Preparing reports...")
        BasicReport.add_logs(reports_objects)
        BasicReport.print_report(reports_objects)

    @staticmethod
    def add_logs(reports):
        print("Writing logs...")
        for report in reports:
            logging.getLogger(BasicReport.LOG).info('{}: Site {}, checked in {} sec, with status {}'
                                              .format(report.test_datetime, report.url, report.time, report.response_code))

    @staticmethod
    def print_report(reports):
        table_rows = []
        for report in reports:
            table_rows.append([report.test_datetime, report.url, report.is_main, report.time, report.time_delta,
                               report.response_code, report.error])

        table = termtables.to_string(
            table_rows,
            header=["DATETIME", "URL", "IS_MAIN", "TIME", "TIME DELTA", "RESPONSE CODE", "ERROR"],
            style="-|+++++++++",
        )
        print(table)
