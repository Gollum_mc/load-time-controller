from time import time
from datetime import datetime

class Timer(object):

    def __enter__(self):
        self.start = time()
        self.test_datetime = datetime.now()
        return self

    def __exit__(self, *args):
        self.end = time()
        self.time_delta = self.end - self.start