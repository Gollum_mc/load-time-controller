from abc import ABC, abstractmethod

from models.report import Report


class Reporter(ABC):

   @staticmethod
   @abstractmethod
   def generate_report(reports_objects):
       pass

   @staticmethod
   def generate_report_data(url_data_dicts):
       from application import caller
       calls_results = caller.get_calls_results(url_data_dicts)
       reports = Reporter.create_reports_objects(calls_results)
       exceeded_times_tuples, twice_exceeded_times_sites_no, main_report = Reporter.process_reports_objects(reports)

       return reports, main_report, exceeded_times_tuples, twice_exceeded_times_sites_no

   @staticmethod
   def create_reports_objects(results_dicts):
       reports = []
       for result_dict in results_dicts:
           report = Report(result_dict['url'], result_dict['is_main'], result_dict['time'], result_dict['test_datetime'],
                           result_dict['code'], result_dict['error'])
           reports.append(report)
       return reports

   @staticmethod
   def process_reports_objects(reports):
       main_report = reports[0]
       comparing_reports = reports[1:]
       exceeded_time_tuples, twice_exceeded_times_sites_no = Reporter.compare_reports_objects(main_report, comparing_reports)
       return exceeded_time_tuples, twice_exceeded_times_sites_no, main_report


   @staticmethod
   def compare_reports_objects(main_report, compared_reports):
       print("Comparing results...")
       exceeded_times_tuples = []
       twice_exceeded_times_sites_no = 0
       main_report.time_delta = 0

       for compared_report in compared_reports:
           exceeded_times_tuple, is_time_exceeded_twice = Reporter.compare_main_report_to_benchmark(main_report, compared_report)
           exceeded_times_tuples, twice_exceeded_times_sites_no = Reporter.set_comparing_results(exceeded_times_tuples,
                                                                                                 twice_exceeded_times_sites_no,
                                                                                                 is_time_exceeded_twice, exceeded_times_tuple)

       return exceeded_times_tuples, twice_exceeded_times_sites_no

   @staticmethod
   def compare_main_report_to_benchmark(main_report, compared_report):
       exceeded_times_tuple = None
       is_time_exceeded_twice = False

       if compared_report.time:
           compared_report.time_delta = compared_report.time - main_report.time
           if main_report.time > compared_report.time and not compared_report.error:
               exceeded_times_tuple = (compared_report.time, compared_report.url)
               is_time_exceeded_twice = Reporter.is_time_exceeded_twice(main_report, compared_report)

       return exceeded_times_tuple, is_time_exceeded_twice

   @staticmethod
   def is_time_exceeded_twice(main_report, compared_report):
       return main_report.time > compared_report.time * 2

   @staticmethod
   def set_comparing_results(exceeded_times_tuples, twice_exceeded_times_sites_no, is_time_exceeded_twice, exceeded_times_tuple):
       if is_time_exceeded_twice:
           twice_exceeded_times_sites_no += 1
       if exceeded_times_tuple:
           exceeded_times_tuples.append(exceeded_times_tuple)

       return exceeded_times_tuples, twice_exceeded_times_sites_no
