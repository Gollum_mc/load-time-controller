from services.notification import SMTPNotification, SMSNotification
from settings import EMAIL_RECIPIENT

EMAIL_SUBJECT = "Slow loading alert"


class AlertManager:
    TEMPLATE_PATH = "templates/email_notification.html"

    @staticmethod
    def send_alert(exceeded_times_tuples, twice_exceeded_times_sites_no, main_report):
        if exceeded_times_tuples:
            AlertManager.send_email_alert(exceeded_times_tuples, main_report)
            if twice_exceeded_times_sites_no > 0:
                AlertManager.send_sms_alert(twice_exceeded_times_sites_no, main_report)

    @staticmethod
    def send_email_alert(exceeded_times_tuples, main_report):
        template = AlertManager.get_email_template(exceeded_times_tuples, main_report)

        notification = SMTPNotification(EMAIL_RECIPIENT, EMAIL_SUBJECT, template)
        notification.send()

    @staticmethod
    def send_sms_alert(twice_exceeded_times_sites_no, main_report):
        sms = SMSNotification("Number of sites loading twice as fast as verified site {}: {}.".format(main_report.url,
                                                                                                      twice_exceeded_times_sites_no))
        sms.send_sms()

    @staticmethod
    def get_list_items(exceeded_times_tuples):
        list_items = ''
        for tuple in exceeded_times_tuples:
            list_items = list_items + "<li>Site {} with time: {}</li>".format(tuple[1], tuple[0])
        return list_items

    @staticmethod
    def get_email_template(exceeded_times_tuples, main_report):
        import codecs
        f = codecs.open(AlertManager.TEMPLATE_PATH, 'r')
        template_content = AlertManager.get_list_items(exceeded_times_tuples)
        html_str = f.read().format(main_report.url, main_report.time, template_content)
        f.close()
        return html_str