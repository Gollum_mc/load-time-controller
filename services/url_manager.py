from re import match
from models.errors import Error


class UrlManager:

    @staticmethod
    def save_main_url():
        urls = []
        url, errors = UrlManager.get_main_url(urls)
        while errors:
            url, errors = UrlManager.get_main_url(urls)
        urls.append(url)
        return urls

    @staticmethod
    def save_benchmark_url(all_urls):
        urls, errors = UrlManager.get_benchmark_urls(all_urls)
        while errors:
            urls, errors = UrlManager.get_benchmark_urls(all_urls)
        all_urls.extend(urls)
        return all_urls

    @staticmethod
    def get_main_url(urls):
        url = input("Type in an URL (in http(s)://example.com format) to be tested:\n")
        errors = UrlManager.verify_url(url, urls)
        Error.print_errors(errors)
        return url, errors

    @staticmethod
    def get_benchmark_urls(all_urls):
        url_str = input("Type in a list of URLs (in http(s)://example.com format, comma separated), "
                        "to be compared:\n")
        urls = UrlManager.get_urls_from_string(url_str)
        errors = []
        for url in urls:
            url_errors = UrlManager.verify_url(url, all_urls)
            errors.extend(url_errors)
        Error.print_errors(errors)
        return urls, errors

    @staticmethod
    def get_urls_from_string(url_str):
        if url_str.find(" ") != -1:
            urls_formatted = url_str.replace(" ", "")
        else:
            urls_formatted = url_str
        urls = urls_formatted.split(",")
        return urls

    @staticmethod
    def verify_url(url, urls):
        errors = []
        if not match(r'https?://(?:[-\w.]|(?:%[\da-fA-F]{2}))+', url):
            errors.append(Error.WRONG_URL.value)

        if url in urls:
            errors.append(Error.URL_ALREADY_ADDED.value)
        return errors

    @staticmethod
    def get_url_data_dicts(urls):
        urls_dicts = [dict(url=urls[0], is_main=True)]
        for url in urls[1:]:
            urls_dicts.append(dict(url=url, is_main=False))
        return urls_dicts