import io
import os
import unittest
import logging
from logger import Logger
from unittest.mock import patch
from datetime import datetime
from services.basic_reporter import BasicReport
from models.report import Report


class TestBasicReporter(unittest.TestCase):
    log_file = None

    @classmethod
    def setUpClass(cls):
        Logger.LOGGERS = [
            (logging.getLogger("reports"), "reports-test", logging.INFO)
        ]

        cls.log_file = open("../logs/reports-test.txt", "w+")

    @classmethod
    def tearDownClass(cls):
        cls.log_file.close()
        os.remove("../logs/reports-test.txt")


    def setUp(self):

        self.main_report = Report(url="google.com", is_main=True, time=2.89, test_datetime=datetime.now(),
                                  response_code=200, error=None, time_delta=0)
        self.compared_report =  Report(url="yahoo.com", is_main=False, time=0.59, test_datetime=datetime.now(),
                                       response_code=200, error=None, time_delta=-0.678)

    @patch("services.basic_reporter.BasicReport.add_logs")
    @patch("services.basic_reporter.BasicReport.print_report")
    def test_generate_report(self, mock_add_log, mock_print_report):

        BasicReport.generate_report([self.main_report, self.compared_report])
        self.assertEqual(mock_add_log.call_count, 1)
        self.assertEqual(mock_print_report.call_count, 1)

    def test_print_report(self):
        with patch('sys.stdout', new=io.StringIO()) as fake_stdout:
            BasicReport.print_report([self.main_report, self.compared_report])
        assert len(fake_stdout.getvalue()) > 5

    def test_add_logs(self):
        Logger.LOGGERS = [
            (logging.getLogger("reports"), "reports", logging.INFO),
            (logging.getLogger("reports-test"), "reports-test", logging.INFO)
        ]
        BasicReport.LOG = 'reports-test'
        Logger.configure_logs()
        BasicReport.add_logs([self.main_report, self.compared_report])
        lines = TestBasicReporter.log_file.readlines()
        self.assertEqual(len(lines), 2)
        self.assertIn(self.main_report.url, lines[0])

if __name__ == "__main__":
    unittest.main()