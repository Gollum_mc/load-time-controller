import unittest
from unittest.mock import patch
from services.reporters import Reporter


class TestReporter(unittest.TestCase):

    def test_generate_report_data(self):
        dicts = [dict(url="http://onet.pl", is_main=True), dict(url="http://op.pl", is_main=False)]
        with patch('services.caller.request.urlopen') as mocked_res:
            mocked_res.return_value.status = 200
            self.assertIsInstance(Reporter.generate_report_data(dicts), tuple)
            self.assertEqual(len(Reporter.generate_report_data(dicts)), 4)

if __name__ == "__main__":
    unittest.main()