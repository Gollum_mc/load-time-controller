import unittest
from services.reporter_factory import ReporterType, ReporterFactory
from services.reporters import Reporter
from services.basic_reporter import BasicReport


class TestReporterFactory(unittest.TestCase):

    def test_get_reporter(self):
        self.assertIsInstance(ReporterFactory.get_reporter(ReporterType.Basic.value), object)
        self.assertIsInstance(ReporterFactory.get_reporter(ReporterType.Basic.value), BasicReport)
        self.assertIsInstance(ReporterFactory.get_reporter(ReporterType.Basic.value), Reporter)
        self.assertIsNone(ReporterFactory.get_reporter("type"))


if __name__ == "__main__":
    unittest.main()