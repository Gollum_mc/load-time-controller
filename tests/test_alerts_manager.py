import unittest
from unittest.mock import patch
from datetime import datetime
from services.alerts_manager import AlertManager
from models.report import Report


class TestAlertsManager(unittest.TestCase):

    def setUp(self):
        self.main_report = Report(url="google.com", is_main=True, time=2.89, test_datetime=datetime.now(),
                                  response_code=200, error=None)
        self.compared_report =  Report(url="yahoo.com", is_main=False, time=0.59, test_datetime=datetime.now(),
                                       response_code=200, error=None)
        self.exceeded_times_tuples = [(0.234, "www.yahoo.com")]

        self.exceeded_times_tuples = [(0.2324, 'http://onet.pl'), (0.344, 'http://gazeta.pl')]
        self.wrong_exceeded_times_tuples = [(0.5667), (0.344, 'http://gazeta.pl')]

    def test_get_list_items_wrong_data(self):
        self.assertRaises(TypeError, AlertManager.get_list_items, None)
        self.assertRaises(TypeError, AlertManager.get_list_items, self.wrong_exceeded_times_tuples )

    def test_get_list_items(self):
        self.assertIsInstance(AlertManager.get_list_items(self.exceeded_times_tuples), str)
        self.assertIn("<li>", AlertManager.get_list_items(self.exceeded_times_tuples))

    @patch("services.notification.SMTPNotification.send")
    def test_send_alert(self, mock_notification_send):

        AlertManager.TEMPLATE_PATH = "../templates/email_notification.html"
        AlertManager.send_alert(self.exceeded_times_tuples, 0, self.main_report)
        self.assertEqual(mock_notification_send.call_count, 1)

    @patch("services.notification.SMTPNotification.send")
    @patch("services.notification.SMSNotification.send_sms")
    def test_send_alert_mock_notification_send_sms(self, mock_notification_send, mock_notification_send_sms):

        AlertManager.TEMPLATE_PATH = "../templates/email_notification.html"
        AlertManager.send_alert(self.exceeded_times_tuples, 1, self.main_report)
        self.assertEqual(mock_notification_send.call_count, 1)
        self.assertEqual(mock_notification_send_sms.call_count, 1)



if __name__ == "__main__":
    unittest.main()