import unittest
from unittest.mock import patch
from models.errors import Error
from application import url_manager


class TestUrlManager(unittest.TestCase):

    def setUp(self):

        self.good_url = 'http://google.com'
        self.wrong_url = "http//o2.pl"
        self.existing_url = 'http://yahoo.com'

        self.url_string = 'http://google.com, http://gazeta.pl, http://op.com'
        self.url_wrong_string = 'http://google.com, http//dd, http://op.com'
        self.url_existing_string = 'http://google.com, http://gazeta.pl, http://yahoo.com'

        self.all_urls = ['http://yahoo.com', 'http://onet.pl']


    def test_get_urls_from_string(self):
        self.assertRaises(AttributeError, url_manager.get_urls_from_string, None)
        self.assertIsInstance(url_manager.get_urls_from_string(self.url_string), list)
        self.assertEqual(len(url_manager.get_urls_from_string(self.url_string)), 3)


    def test_verify_url(self):
        self.assertRaises(TypeError, url_manager.verify_url, None)

        self.assertIsInstance(url_manager.verify_url(self.good_url, self.all_urls), list)
        self.assertEqual(url_manager.verify_url(self.good_url, self.all_urls), [])

        self.assertEqual(len(url_manager.verify_url(self.wrong_url, self.all_urls)), 1)
        self.assertEqual(url_manager.verify_url(self.wrong_url, self.all_urls)[0], Error.WRONG_URL.value)

        self.assertEqual(url_manager.verify_url(self.existing_url, self.all_urls)[0], Error.URL_ALREADY_ADDED.value)


    def test_get_url_data_dicts(self):
        self.assertIsNotNone(url_manager.get_url_data_dicts(self.all_urls))
        self.assertIsInstance(url_manager.get_url_data_dicts(self.all_urls), list)
        self.assertEqual(len(url_manager.get_url_data_dicts(self.all_urls)), 2)
        self.assertIsInstance(url_manager.get_url_data_dicts(self.all_urls)[0], dict)
        self.assertIn('is_main', url_manager.get_url_data_dicts(self.all_urls)[0])
        self.assertIn('url', url_manager.get_url_data_dicts(self.all_urls)[0])
        self.assertEqual(url_manager.get_url_data_dicts(self.all_urls)[0]['is_main'], True)
        self.assertEqual(url_manager.get_url_data_dicts(self.all_urls)[1]['is_main'], False)
        self.assertEqual(url_manager.get_url_data_dicts(self.all_urls)[0]['url'], self.all_urls[0])
        self.assertEqual(url_manager.get_url_data_dicts(self.all_urls)[1]['url'], self.all_urls[1])

        self.all_urls = []

        self.assertRaises(IndexError, url_manager.get_url_data_dicts, self.all_urls)

    def test_get_main_url_good_url(self):
        with patch('services.url_manager.input') as mocked_input:

            mocked_input.return_value = self.good_url

            self.assertIsInstance(url_manager.get_main_url(self.all_urls), tuple)
            self.assertEqual(len(url_manager.get_main_url(self.all_urls)), 2)
            self.assertEqual(url_manager.get_main_url(self.all_urls)[0], self.good_url)
            self.assertEqual(url_manager.get_main_url(self.all_urls)[1], [])

    def test_get_main_url_wrong_url(self):
        with patch('services.url_manager.input') as mocked_input:

            mocked_input.return_value = self.wrong_url

            self.assertIsInstance(url_manager.get_main_url(self.all_urls), tuple)
            self.assertEqual(len(url_manager.get_main_url(self.all_urls)), 2)
            self.assertEqual(url_manager.get_main_url(self.all_urls)[0], self.wrong_url)
            self.assertEqual(url_manager.get_main_url(self.all_urls)[1], [Error.WRONG_URL.value])

    def test_get_main_url_existing_url(self):
        with patch('services.url_manager.input') as mocked_input:

            mocked_input.return_value = self.existing_url

            self.assertIsInstance(url_manager.get_main_url(self.all_urls), tuple)
            self.assertEqual(len(url_manager.get_main_url(self.all_urls)), 2)
            self.assertEqual(url_manager.get_main_url(self.all_urls)[0], self.existing_url)
            self.assertEqual(url_manager.get_main_url(self.all_urls)[1], [Error.URL_ALREADY_ADDED.value])


    def test_get_benchmark_urls_good_url(self):
        with patch('services.url_manager.input') as mocked_input:

            mocked_input.return_value = self.url_string

            self.assertIsInstance(url_manager.get_benchmark_urls(self.all_urls), tuple)
            self.assertEqual(len(url_manager.get_benchmark_urls(self.all_urls)), 2)
            self.assertEqual(url_manager.get_benchmark_urls(self.all_urls)[0][0], self.good_url)
            self.assertEqual(url_manager.get_benchmark_urls(self.all_urls)[1], [])

    def test_get_benchmark_urls_wrong_url(self):
        with patch('services.url_manager.input') as mocked_input:

            mocked_input.return_value = self.url_wrong_string

            self.assertIsInstance(url_manager.get_benchmark_urls(self.all_urls), tuple)
            self.assertEqual(len(url_manager.get_benchmark_urls(self.all_urls)), 2)
            self.assertEqual(url_manager.get_benchmark_urls(self.all_urls)[0][0], self.good_url)
            self.assertEqual(url_manager.get_benchmark_urls(self.all_urls)[1], [Error.WRONG_URL.value])

    def test_get_benchmark_urls_existing_url(self):
        with patch('services.url_manager.input') as mocked_input:

            mocked_input.return_value = self.url_existing_string

            self.assertIsInstance(url_manager.get_benchmark_urls(self.all_urls), tuple)
            self.assertEqual(len(url_manager.get_benchmark_urls(self.all_urls)), 2)
            self.assertEqual(url_manager.get_benchmark_urls(self.all_urls)[0][0], self.good_url)
            self.assertEqual(list(set(url_manager.get_benchmark_urls(self.all_urls)[1])), [Error.URL_ALREADY_ADDED.value])


    def test_save_main_url_multiple_values(self):
        with patch('services.url_manager.input') as mocked_input:

            mocked_input.side_effect = [self.wrong_url, self.good_url, self.existing_url]


            self.assertEqual(len(url_manager.save_main_url()), 1)
            self.assertEqual(url_manager.save_main_url()[0], self.existing_url)


    def test_save_benchmark_url_multiple_values(self):
        with patch('services.url_manager.input') as mocked_input:

            mocked_input.side_effect = [self.url_wrong_string, self.url_existing_string, self.url_string]
            self.all_urls = ['http://yahoo.com']

            url_manager.save_benchmark_url(self.all_urls)

            self.assertEqual(len(self.all_urls), 4)
            self.assertEqual(self.all_urls[0], 'http://yahoo.com')
            self.assertEqual(self.all_urls[-1], 'http://op.com')




if __name__ == "__main__":
    unittest.main()