import io
import unittest
from unittest.mock import patch
from models.errors import Error



class TestError(unittest.TestCase):

    def test_print_errors(self):
        with patch('sys.stdout', new=io.StringIO()) as fake_stdout:
            Error.print_errors([Error.WRONG_URL.value])
        assert fake_stdout.getvalue()[5:-5] == Error.WRONG_URL.value


if __name__ == "__main__":
    unittest.main()