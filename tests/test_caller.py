import unittest
from unittest.mock import patch
from services.caller import Caller
from urllib import error as urllib_error


class TestCaller(unittest.TestCase):

    def setUp(self):

        self.url = 'http://google.com'
        self.wrong_url = 'http//op'

    def test_is_network_connection_failure(self):
        with patch('services.caller.request.urlopen') as mocked_res:
            mocked_res.side_effect = urllib_error.URLError("test")

            self.assertEqual(Caller.is_network_connection(), False)

    def test_is_network_connection_success(self):
        with patch('services.caller.request.urlopen') as mocked_res:
            mocked_res.return_value = "test"

            self.assertEqual(Caller.is_network_connection(), True)

    def test_call_url_success(self):
        with patch('services.caller.request.urlopen') as mocked_res:
            mocked_res.return_value = "test"

            self.assertIsInstance(Caller.call_url(self.url), tuple)
            self.assertEqual(len(Caller.call_url(self.url)), 4)
            self.assertIsNone(Caller.call_url(self.url)[3])

    def test_call_url_not_responding(self):
        with patch('services.caller.request.urlopen') as mocked_res:
            mocked_res.side_effect = urllib_error.URLError("test")

            self.assertIsInstance(Caller.call_url(self.wrong_url), tuple)
            self.assertEqual(len(Caller.call_url(self.wrong_url)), 4)
            self.assertIsNotNone(Caller.call_url(self.url)[3])

    def test_get_calls_results(self):
        dicts = [dict(url="http://onet.pl", is_main=True), dict(url="http://op.pl", is_main=False)]

        with patch('services.caller.request.urlopen') as mocked_res:
            mocked_res.return_value.status = 200

            self.assertIsInstance(Caller.get_calls_results(dicts), list)
            self.assertEqual(mocked_res.call_count, len(dicts))


if __name__ == "__main__":
    unittest.main()