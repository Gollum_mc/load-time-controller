import os

from setuptools import setup, find_packages

here = os.path.abspath(os.path.dirname(__file__))

with open(os.path.join(here, 'README.md')) as f:
    README = f.read()

requires = [
    'smsapi-client',
    'termcolor',
    'termtables'
    ]

setup(name='Load Time Controller',
      version='1.0',
      description='',
      long_description=README,
      classifiers=[
        "Programming Language :: Python",
        ],
      author='Marta Chludzińska',
      author_email='marta.chludzinska@yahoo.com',
      packages=find_packages(),
      include_package_data=True,
      zip_safe=False,
      install_requires=requires,
      )